# README #

#### Images 
##### Unit test coverage no less than 80%
![test-coverage](https://bitbucket.org/carlosdominguez/carlos-golang-encryptor/raw/master/resources/img/test-coverage.PNG)

##### Health endpoint
![api-health](https://bitbucket.org/carlosdominguez/carlos-golang-encryptor/raw/master/resources/img/api-health.PNG)

##### Sending an encrypt request
![api-encrypt](https://bitbucket.org/carlosdominguez/carlos-golang-encryptor/raw/master/resources/img/api-encrypt.PNG)

##### Sending a decryp request
![api-decrypt](https://bitbucket.org/carlosdominguez/carlos-golang-encryptor/raw/master/resources/img/api-decrypt.PNG)

##### Errors
![errors](https://bitbucket.org/carlosdominguez/carlos-golang-encryptor/raw/master/resources/img/errors.PNG)

#### Run docker image (Already pushed into Docker hub)
```bash
docker run -d -p 8000:8000 krlosnando/carlos-golang-encryptor:latest

# Go to your browser and test http://localhost:8000/api/health
```

#### Test in linux terminal
```bash
# Test using curl
curl localhost:8000/api/health
curl -X POST localhost:8000/api/encrypt --data '{"Value": "My super encryptor!"}'
curl -X POST localhost:8000/api/decrypt --data '{"Value": "TXkgc3VwZXIgZW5jcnlwdG9yIQ=="}'
```

#### Build docker image (Go to root of git repo and run cmd)
```bash
docker build --rm -f "Dockerfile" -t krlosnando/carlos-golang-encryptor:latest .
```

#### Use build env with docker-compose (Go to root of git repo and run cmd)
```bash
docker-compose --build -d
```