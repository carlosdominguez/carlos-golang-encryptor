FROM golang:1.14-alpine

# Install additional packages
RUN apk update \
	&& apk add bash \
	&& apk add git \
	&& apk add openssh-client \
	&& apk add jq \
	&& apk add curl\
	&& apk add gcc \
	&& apk add alpine-sdk

WORKDIR /usr/src/golang-encryptor
COPY ./app.go /usr/src/golang-encryptor/app.go
COPY ./app_test.go /usr/src/golang-encryptor/app_test.go

# Install app packages
RUN cd /usr/src/golang-encryptor \
	&& go get -u github.com/gorilla/mux \
	&& go get -u github.com/gorilla/handlers

# Run application
CMD ["go", "run", "app.go"]