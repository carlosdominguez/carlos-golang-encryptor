package main

import (

	// Used to simulate a simple encrypt and decrypt process
	b64 "encoding/base64"

	// Create simple http server	
	"net/http"
	
	// Print errors on console
	"log"

	// Encode to json
	"encoding/json"

	// External packages handlers to enable CORS
	"github.com/gorilla/handlers"

	// External package to create simple REST API
    "github.com/gorilla/mux"
)

// Document with the value to decryp and encrypt
type Document struct {
	Value string
}

// ResponseResult struct to respond to any http request
type ResponseResult struct {
	Input string
	Result string
	Status string
	Message string
}

/*!
 * @function    Function to encrypt, using base 64 to simulate a simple encrypt process of string
 * @param       data    String to encrypt.
 * @result      Encrypted string
*/
func Encrypt(data string) string {
	sEnc := b64.StdEncoding.EncodeToString([]byte(data))
	return sEnc
}

/*!
 * @function    Function to decrypt, using base 64 to simulate a simple decrypt process of string
 * @param       data    String to decrypt.
 * @result      Decrypted string
*/
func Decrypt(data string) string {
	sDec, _ := b64.StdEncoding.DecodeString(data)
	return string(sDec)
}

/*!
 * @function    Function to handle route (localhost:8000/api/encrypt) to encrypt a string
 * @param       w    http ResponseWriter.
 * @param       r    http request
*/
func RouteEncrypt(w http.ResponseWriter, r *http.Request) {
	
	//Create Doc Struct
    var doc Document
    json.NewDecoder(r.Body).Decode(&doc)
	
	//Create ResponseResult
	var res ResponseResult

	//Set json response header
	w.Header().Set("Content-Type", "application/json")

	// check to prevent process empty values
    if doc.Value != "" {
		res.Input = doc.Value
		res.Result = Encrypt(doc.Value)
		res.Status = "success"
		w.WriteHeader(http.StatusOK)
    } else {
		res.Input = doc.Value
		res.Result = ""
		res.Status = "error"
		res.Message = "Input cannot be empty"
		w.WriteHeader(http.StatusBadRequest)
	}

	// parse to json the response
	resJson, err := json.Marshal(res)
	_ = err

	w.Write([]byte(resJson))
}

/*!
 * @function    Function to handle route (localhost:8000/api/decrypt) to decrypt a string
 * @param       w    http ResponseWriter.
 * @param       r    http request
*/
func RouteDecrypt(w http.ResponseWriter, r *http.Request) {

	//Create Doc Struct
    var doc Document
    json.NewDecoder(r.Body).Decode(&doc)
	
	//Create ResponseResult
	var res ResponseResult

	//Set json response header
	w.Header().Set("Content-Type", "application/json")
	
	// check to prevent process empty values
    if doc.Value != "" {
		res.Input = doc.Value
		res.Result = Decrypt(doc.Value)
		res.Status = "success"
		w.WriteHeader(http.StatusOK)
    } else {
		res.Input = doc.Value
		res.Result = ""
		res.Status = "error"
		res.Message = "Input cannot be empty"
		w.WriteHeader(http.StatusBadRequest)
	}

	// parse to json the response
	resJson, err := json.Marshal(res)
	_ = err

	w.Write([]byte(resJson))
}

/*!
 * @function    Function to handle route (localhost:8000/api/health) to check Health of application
 * @param       w    http ResponseWriter.
 * @param       r    http request
*/
func RouteHealth(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Awesome Carlos's REST API Working!"))
}

/*!
 * @function    main funciton
*/
func main() {
	router := mux.NewRouter()
	
	// Configuring CORS headers
	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With"})
    originsOk := handlers.AllowedOrigins([]string{"*"})
    methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS"})

	// Routes consist of a path and a handler function.
	router.HandleFunc("/api/encrypt", RouteEncrypt).Methods("POST", "OPTIONS")
	
	// Routes consist of a path and a handler function.
    router.HandleFunc("/api/decrypt", RouteDecrypt).Methods("POST", "OPTIONS")

	// Routes consist of a path and a handler function.
	router.HandleFunc("/api/health", RouteHealth).Methods("GET")
	
    // Bind to a port and pass our router in
    log.Fatal(http.ListenAndServe(":8000", handlers.CORS(headersOk, originsOk, methodsOk)(router)))
}