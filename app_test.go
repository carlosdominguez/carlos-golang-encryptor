package main

import (
    "strings"
    "net/http"
    "net/http/httptest"
    "testing"
)

type Test struct {
    in  string
    out string
}

func TestEncrypt(t *testing.T) {
    var tests = []Test{
        {"My super encryptor!", "TXkgc3VwZXIgZW5jcnlwdG9yIQ=="},
        {"Learning Golang XD!", "TGVhcm5pbmcgR29sYW5nIFhEIQ=="},
    }

    for i, test := range tests {
        result := Encrypt(test.in)
        if result != test.out {
            t.Errorf("#%d: Encrypt(%s)=%s; want %s", i, test.in, result, test.out)
        }
    }
}

func TestDecrypt(t *testing.T) {
    var tests = []Test{
        {"TXkgc3VwZXIgZW5jcnlwdG9yIQ==", "My super encryptor!"},
        {"TGVhcm5pbmcgR29sYW5nIFhEIQ==", "Learning Golang XD!"},
    }

    for i, test := range tests {
        result := Decrypt(test.in)
        if result != test.out {
            t.Errorf("#%d: Decrypt(%s)=%s; want %s", i, test.in, result, test.out)
        }
    }
}

func TestEncryptRouteHandlerError(t *testing.T) {
    // Create a request to pass to our handler. 
    req, err := http.NewRequest(http.MethodPost, "/api/encrypt", strings.NewReader(`{"Value": ""}`))
    if err != nil {
        t.Fatal(err)
    }

    // We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
    rr := httptest.NewRecorder()
    handler := http.HandlerFunc(RouteEncrypt)

    // Our handlers satisfy http.Handler, so we can call their ServeHTTP method
    // directly and pass in our Request and ResponseRecorder.
    handler.ServeHTTP(rr, req)

    // Check the status code is what we expect.
    if status := rr.Code; status != http.StatusBadRequest {
        t.Errorf("handler returned wrong status code: got %v want %v",
            status, http.StatusBadRequest)
    }

    // Check the response body is what we expect.
    expected := `{"Input":"","Result":"","Status":"error","Message":"Input cannot be empty"}`
    if rr.Body.String() != expected {
        t.Errorf("handler returned unexpected body: got %v want %v",
            rr.Body.String(), expected)
    }
}

func TestDecryptRouteHandlerError(t *testing.T) {
    // Create a request to pass to our handler.
    req, err := http.NewRequest(http.MethodPost, "/api/decrypt", strings.NewReader(`{"Value": ""}`))
    if err != nil {
        t.Fatal(err)
    }

    // We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
    rr := httptest.NewRecorder()
    handler := http.HandlerFunc(RouteDecrypt)

    // Our handlers satisfy http.Handler, so we can call their ServeHTTP method
    // directly and pass in our Request and ResponseRecorder.
    handler.ServeHTTP(rr, req)

    // Check the status code is what we expect.
    if status := rr.Code; status != http.StatusBadRequest {
        t.Errorf("handler returned wrong status code: got %v want %v",
            status, http.StatusBadRequest)
    }

    // Check the response body is what we expect.
    expected := `{"Input":"","Result":"","Status":"error","Message":"Input cannot be empty"}`
    if rr.Body.String() != expected {
        t.Errorf("handler returned unexpected body: got %v want %v",
            rr.Body.String(), expected)
    }
}

func TestEncryptRouteHandlerSuccess(t *testing.T) {
    // Create a request to pass to our handler. 
    req, err := http.NewRequest(http.MethodPost, "/api/encrypt", strings.NewReader(`{"Value": "My super encryptor!"}`))
    if err != nil {
        t.Fatal(err)
    }

    // We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
    rr := httptest.NewRecorder()
    handler := http.HandlerFunc(RouteEncrypt)

    // Our handlers satisfy http.Handler, so we can call their ServeHTTP method
    // directly and pass in our Request and ResponseRecorder.
    handler.ServeHTTP(rr, req)

    // Check the status code is what we expect.
    if status := rr.Code; status != http.StatusOK {
        t.Errorf("handler returned wrong status code: got %v want %v",
            status, http.StatusOK)
    }

    // Check the response body is what we expect.
    expected := `{"Input":"My super encryptor!","Result":"TXkgc3VwZXIgZW5jcnlwdG9yIQ==","Status":"success","Message":""}`
    if rr.Body.String() != expected {
        t.Errorf("handler returned unexpected body: got %v want %v",
            rr.Body.String(), expected)
    }
}

func TestDecryptRouteHandlerSuccess(t *testing.T) {
    // Create a request to pass to our handler.
    req, err := http.NewRequest(http.MethodPost, "/api/decrypt", strings.NewReader(`{"Value": "TXkgc3VwZXIgZW5jcnlwdG9yIQ=="}`))
    if err != nil {
        t.Fatal(err)
    }

    // We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
    rr := httptest.NewRecorder()
    handler := http.HandlerFunc(RouteDecrypt)

    // Our handlers satisfy http.Handler, so we can call their ServeHTTP method
    // directly and pass in our Request and ResponseRecorder.
    handler.ServeHTTP(rr, req)

    // Check the status code is what we expect.
    if status := rr.Code; status != http.StatusOK {
        t.Errorf("handler returned wrong status code: got %v want %v",
            status, http.StatusOK)
    }

    // Check the response body is what we expect.
    expected := `{"Input":"TXkgc3VwZXIgZW5jcnlwdG9yIQ==","Result":"My super encryptor!","Status":"success","Message":""}`
    if rr.Body.String() != expected {
        t.Errorf("handler returned unexpected body: got %v want %v",
            rr.Body.String(), expected)
    }
}

func TestHealthRouteHandlerSuccess(t *testing.T) {
    // Create a request to pass to our handler.
    req, err := http.NewRequest(http.MethodPost, "/api/decrypt", strings.NewReader(``))
    if err != nil {
        t.Fatal(err)
    }

    // We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
    rr := httptest.NewRecorder()
    handler := http.HandlerFunc(RouteHealth)

    // Our handlers satisfy http.Handler, so we can call their ServeHTTP method
    // directly and pass in our Request and ResponseRecorder.
    handler.ServeHTTP(rr, req)

    // Check the status code is what we expect.
    if status := rr.Code; status != http.StatusOK {
        t.Errorf("handler returned wrong status code: got %v want %v",
            status, http.StatusOK)
    }

    // Check the response body is what we expect.
    expected := `Awesome Carlos's REST API Working!`
    if rr.Body.String() != expected {
        t.Errorf("handler returned unexpected body: got %v want %v",
            rr.Body.String(), expected)
    }
}